# Gemstone

Gemstone is an open-source app for time management and biometric tracking. Goals include:
- Easy data export to a variety of common formats
- Custom data storage location, encrypted and secure storage
- Quick data entry on the go, with deferred uploads if no connection
- Time management and tracking, schedule integration
- Journaling
- Biometric tracking, including:
    - Weight
    - Nutrition (both calories and nutrients)
    - Sleep
    - Exercise (including GPS integration for walking/jogging/running)
- Intelligent handling of data-entry errors and margins of confidence
- Option to publish anonymized data for research purposes